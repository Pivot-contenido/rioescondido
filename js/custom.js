$('.navbar-nav .nav-item').click(function () {
    $('.navbar-nav .nav-item.active').removeClass('active');
    $(this).addClass('active');
});

$(document).on("click", ".nav-item", function () {
    jQuery(".nav-item").closest(".bsnav-mobile").removeClass("in");
    jQuery(".toggler-spring").removeClass("active");
});

$(window).scroll(function () {
    var href = $(this).scrollTop();
    $('.link').each(function (event) {
        if (href >= $($(this).attr('href')).offset().top - 1) {
            $('.navbar-nav .nav-item.active').removeClass('active');
            $(this).addClass('active');
        }
    });
});

AOS.init();


$(document).on('scroll', function () {
    if ($(window).scrollTop() > 50) {
      $('.menu').addClass('bg-white nav-shadow');
      $(".nav-link ").addClass("brown-links");
      $(".navbar-toggler-icon").addClass("blue-toggler");
    }
    else {
        $(".menu").removeClass("bg-white nav-shadow");
        $(".nav-link").removeClass("brown-links");
        $(".navbar-toggler-icon").removeClass("blue-toggler");
    }
});


$(document).on('click', 'a[href^="#"]', function (event) {
    event.preventDefault();

    $('html, body').animate({
        scrollTop: $($.attr(this, 'href')).offset().top - 120 
    });
});